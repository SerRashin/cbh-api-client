<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src'),
    'PhpCollection' => array($vendorDir . '/phpcollection/phpcollection/src'),
    'PHPSQLParser\\' => array($vendorDir . '/greenlion/php-sql-parser/src'),
    'Metadata\\' => array($vendorDir . '/jms/metadata/src'),
    'JMS\\Serializer' => array($vendorDir . '/jms/serializer/src'),
    'JMS\\' => array($vendorDir . '/jms/parser-lib/src'),
    'Doctrine\\ORM\\' => array($vendorDir . '/doctrine/orm/lib'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Circle\\RestClientBundle' => array($vendorDir . '/ci/restclientbundle'),
    'Circle\\DoctrineRestDriver' => array($vendorDir . '/circle/doctrine-rest-driver'),
    'Analog' => array($vendorDir . '/analog/analog/lib'),
);
