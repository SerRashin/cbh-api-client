<?php

use CBH\API\Entity\Client;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver;
use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Doctrine\ORM\Tools\Setup;

require 'vendor/autoload.php';

//try {
//    $apiClient = new \CBH\API\APIClient([
//        'application_name'   => 'CBH Client',
//        'application_id'     => 'trewrt trew trwert ewrt qwerty',
//        'application_secret' => '1qaz2wsx3edc4rfv',
////        'api_url'            => 'dwqerweqrwq',
////        'api_version'        => 'dwqerweqrwq',
//    ]);
//} catch (\CBH\API\Exception\APIClientException $e) {
//    die($e->getMessage());
//}
//
//$requestFactory = new \CBH\API\Factory\RequestFactory($apiClient);
//
////$apiClient->get();
////$apiClient->post();
//
////
//
////
//$clientRepository = new \CBH\API\Repository\ClientRepository($requestFactory);
//
//$clientService = new \CBH\API\Service\ClientService($clientRepository);
////
////
////
//////// usage in controller
//$client = new \CBH\API\ValueObject\RegistrationCredentials(
//    'ser@rashin.me',
//    'secretpwd'
//);
////
////$client = $clientService->registerClient($client);
//
//
//$clientService->registerClient($client);


//$doctrineDriver = new \Circle\DoctrineRestDriver\Driver();
//
//$doctrineDriver->connect([
//    'host'     => 'api.cbh.loc',
//    'port'     => '80',
//    'user'     => 'username',
//    'password' => 'password',
//    'options'  => [
//        'format'                         => "json",
//        'authenticator_class'            => 'HttpAuthentication',
//        'CURLOPT_CURLOPT_FOLLOWLOCATION' => true,
//        'CURLOPT_HEADER'                 => true,
//    ],
//]);

///// сделать сервис

$config = new \Doctrine\ORM\Configuration();

$namespaces = array(
    "src/Resources/mapping" => 'CBH\API\Entity',
);

$driver = new SimplifiedYamlDriver($namespaces);
$driver->setGlobalBasename('global');


$config->setMetadataDriverImpl($driver);
$config->setProxyDir(__DIR__ . '/Proxies');
$config->setProxyNamespace('Proxies');

$connectionOptions = [
    'driverClass'=> '\Circle\DoctrineRestDriver\Driver',
    'host'     => 'http://api.cbh.loc/api/v2',
    'port'     => '80',
    'user'     => 'username',
    'password' => 'password',
    'driverOptions'  => [
        'format'                         => "json",
        'authenticator_class'            => 'HttpAuthentication',
    ],
];

$em = EntityManager::create($connectionOptions, $config);


///// сделать сервис

$client = $em->getRepository(Client::class)->findOneBy(['id'=>1]);
var_dump($client);


