<?php

declare(strict_types = 1);

namespace CBH\API\Service;

use CBH\API\Entity\Client;
use CBH\API\Repository\ClientRepository;
use CBH\API\Entity\ClientInterface;
use CBH\API\ValueObject\RegistrationCredentialsInterface;

/**
 * Сервис клиента
 *
 * @package CBH\API\Service
 */
class ClientService
{
    /**
     * Репозиторий клиента
     *
     * @var ClientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Регистрация клиента
     *
     * @param RegistrationCredentialsInterface $registrationCredentials
     *
     * @return ClientInterface
     */
    public function registerClient(RegistrationCredentialsInterface $registrationCredentials)//: ClientInterface
    {
        $client = new Client();

        $client->setEmail($registrationCredentials->getEmail());
        $client->setPassword($registrationCredentials->getPassword());

        $this->clientRepository->add($client);



        return $client;
    }
}
