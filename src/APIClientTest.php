<?php

declare(strict_types = 1);

namespace CBH\API;

use CBH\API\Exception\APIClientException;
use PHPUnit\Framework\TestCase;

/**
 * Тест API Клиента CBH
 *
 * @package CBH\API
 */
class APIClientTest extends TestCase
{
    /**
     * Тестовые данные
     *
     * @var array
     */
    private $testData;

    protected function setUp()
    {
        parent::setUp();

        $this->testData = [
            'application_name'   => 'Some Name',
            'application_id'     => 'Some Id',
            'application_secret' => 'Some Secret',
            'api_url'            => 'https://callbackhunter.com/api/v2/',
            'timeout'            => 10,
        ];
    }

    /**
     * Установка имени приложения
     */
    public function testApplicationName()
    {
        $client = new APIClient($this->testData);

        $this->assertSame($this->testData['application_name'], $client->getApplicationName());
    }

    /**
     * Установка ID приложения
     */
    public function testApplicationId()
    {
        $client = new APIClient($this->testData);

        $this->assertSame($this->testData['application_id'], $client->getApplicationId());
    }

    /**
     * Установка секретного ключа приложения
     */
    public function testApplicationSecret()
    {
        $client = new APIClient($this->testData);

        $this->assertSame($this->testData['application_secret'], $client->getApplicationSecret());
    }

    /**
     * Проверка поведения, если не указан хотябы один из параметров приложения
     */
    public function testBehaviorIfApplicationConfigsNotSet()
    {
        unset($this->testData['application_name']);
        unset($this->testData['application_id']);
        unset($this->testData['application_secret']);

        $this->expectException(APIClientException::class);

        new APIClient($this->testData);
    }

    /**
     * Проверка значения API URL по умолчанию
     */
    public function testApiUrlDefault()
    {
        unset($this->testData['api_url']);
        $client = new APIClient($this->testData);

        $this->assertSame(APIClient::API_URL, $client->getApiUrl());
    }

    /**
     * Установка Url API сервера
     */
    public function testApiUrl()
    {
        $client = new APIClient($this->testData);

        $this->assertSame($this->testData['api_url'], $client->getApiUrl());
    }

    /**
     * Проверка задерки запроса (timeout) по умолчанию
     */
    public function testTimeoutDefault()
    {
        unset($this->testData['timeout']);
        $client = new APIClient($this->testData);

        $this->assertSame(APIClient::TIMEOUT, $client->getTimeout());
    }

    /**
     * Установка задерки запроса (timeout)
     */
    public function testTimeout()
    {
        $client = new APIClient($this->testData);

        $this->assertSame($this->testData['timeout'], $client->getTimeout());
    }
}
