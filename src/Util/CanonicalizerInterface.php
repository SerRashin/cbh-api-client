<?php

declare(strict_types = 1);

namespace CBH\API\Util;

interface CanonicalizerInterface
{
    /**
     * Сделать строку канонической (согластно какому-либо образцу)
     *
     * @param string $string
     *
     * @return string
     */
    public function canonicalize($string);
}