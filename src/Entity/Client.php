<?php

declare(strict_types = 1);

namespace CBH\API\Entity;

use CBH\API\Entity\ClientInterface;
use DateTime;

/**
 * Клиент
 *
 * @package CBH\API\Entity
 */
class Client implements ClientInterface
{
    /**
     * ID пользователя
     *
     * @var int
     */
    private $id = 0;

    /**
     * Имя
     *
     * @var string
     */
    private $firstName = '';

    /**
     * Фамилия
     *
     * @var string
     */
    private $lastName = '';

    /**
     * E-mail пользователя
     *
     * @var string
     */
    private $email = '';

    /**
     * Пол
     *
     * @var string
     */
    private $gender = '';

    /**
     * Дата рождения
     *
     * @var DateTime
     */
    private $dateOfBirth;

    /**
     * Если пользователь включен
     *
     * @var bool
     */
    private $enabled = '';

    /**
     * Пароль пользователя
     *
     * @var string
     */
    private $password = '';

    /**
     * Дата последнего входа пользователя
     *
     * @var DateTime
     */
    private $lastLoginDate;

    /**
     * Дата регистрации
     *
     * @var DateTime
     */
    private $registrationDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return DateTime
     */
    public function getDateOfBirth(): DateTime
    {
        return $this->dateOfBirth;
    }

    /**
     * @param DateTime $dateOfBirth
     */
    public function setDateOfBirth(DateTime $dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return DateTime
     */
    public function getLastLoginDate(): DateTime
    {
        return $this->lastLoginDate;
    }

    /**
     * @return DateTime
     */
    public function getRegistrationDate(): DateTime
    {
        return $this->registrationDate;
    }
}
