<?php

declare(strict_types = 1);

namespace CBH\API\ValueObject;

/**
 * Данные для регистрации клиента
 */
class RegistrationCredentials implements RegistrationCredentialsInterface
{
    /**
     * E-mail
     *
     * @var string
     */
    private $email;
    /**
     * Пароль
     *
     * @var string
     */
    private $password;

    /**
     * Конструктор
     *
     * @param string $email
     * @param string $password
     */
    public function __construct(string $email, string $password)
    {
        $this->email    = $email;
        $this->password = $password;
    }
    /**
     * Возвращает E-mail
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    /**
     * Возвращает пароль
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}