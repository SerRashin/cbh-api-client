<?php

declare(strict_types = 1);

namespace CBH\API\ValueObject;

/**
 * Интерфейс для регистрации нового клиента
 *
 * @package CBH\API\ValueObject
 */
interface RegistrationCredentialsInterface
{
    /**
     * Получить E-mail клиента
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     * Получить plain-text пароль
     *
     * @return string
     */
    public function getPassword(): string;
}