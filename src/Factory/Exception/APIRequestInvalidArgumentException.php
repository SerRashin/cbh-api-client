<?php

declare(strict_types = 1);

namespace CBH\API\Factory;

class APIRequestInvalidArgumentException  extends \InvalidArgumentException
{
    /**
     * @param string $context
     * @param mixed  $given
     * @param int    $parameterIndex
     *
     * @return APIRequestInvalidArgumentException
     */
    public static function invalidObject($context, $given, $parameterIndex = 1)
    {
        return new self($context . ' expects parameter ' . $parameterIndex .
            ' to be an entity object, '. gettype($given) . ' given.');
    }
}
