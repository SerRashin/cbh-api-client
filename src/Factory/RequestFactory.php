<?php

declare(strict_types = 1);

namespace CBH\API\Factory;


use CBH\API\APIClient;

class RequestFactory
{
    /**
     * @var APIClient
     */
    private $client;

    public function __construct(APIClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param object $entity
     *
     * @throws \CBH\API\Factory\APIRequestInvalidArgumentException
     */
    public function add($entity)
    {
        if ( ! is_object($entity)) {
            throw APIRequestInvalidArgumentException::invalidObject('EntityManager#persist()' , $entity);
        }

        $clients = 'clients';

        $postData = (array) $entity;

        $this->client->add($clients, $postData);


       // var_dump($entity);
    }

    private function unitOfWork()
    {
       // unitOfWork
    }
}
