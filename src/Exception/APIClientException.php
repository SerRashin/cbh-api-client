<?php

declare(strict_types = 1);

namespace CBH\API\Exception;

use Exception;

/**
 * Class APIClientException
 *
 * @package CBH\API\Exception
 */
class APIClientException extends Exception
{

}
