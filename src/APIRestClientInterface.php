<?php

declare(strict_types = 1);

namespace CBH\API;

/**
 * Интерфейс API клиента для отправки запросов
 *
 * @package CBH\API
 */
interface APIRestClientInterface
{
    /**
     * Получить данные [GET]
     *
     * @param string $uri
     * @param array $filters
     */
    public function get(string $uri, array $filters = []);

    /**
     * Добавить запись на API сервер [POST]
     *
     * @param string $uri
     * @param array $data
     */
    public function add(string $uri, array $data = []);

    /**
     * Редактирование записи [POST]
     *
     * @param string $uri
     * @param array $data
     */
    public function edit(string $uri, array $data = []);

    /**
     * Удаление записи [DELETE]
     *
     * @param string $uri
     * @param array $data
     */
    public function delete(string $uri, array $data = []);
}