<?php

declare(strict_types = 1);

namespace CBH\API\Repository;

use CBH\API\Entity\ClientInterface;
use CBH\API\Factory\RequestFactory;

/**
 * Репозиторий клиента
 *
 * @package CBH\API\Repository
 */
class ClientRepository
{
    /**
     * Фабрика запросов
     *
     * @var RequestFactory
     */
    private $requestFactory;

    public function __construct(RequestFactory $requestFactory)
    {
        $this->requestFactory = $requestFactory;
    }

    /**
     * Добавляет пользователя
     *
     * @param ClientInterface $client
     */
    public function add(ClientInterface $client)
    {
        $this->requestFactory->add($client);
    }
}