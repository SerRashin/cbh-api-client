<?php

declare(strict_types = 1);

namespace CBH\API;

use CBH\API\Exception\APIClientException;
use GuzzleHttp\Client;

/**
 * Class APIClient
 *
 * @package CBH\API
 */
class APIClient extends Client implements APIRestClientInterface
{
    const API_URL = 'http://api.cbh.loc/api/v2/';

    const TIMEOUT = 0;

    /**
     * Название вашего приложения
     *
     * @var string
     */
    private $applicationName;

    /**
     * Id вашего приложения
     *
     * @var string
     */
    private $applicationId;

    /**
     * Секретный ключ вашего приложения
     *
     * @var string
     */
    private $applicationSecret;

    /**
     * URL API
     *
     * @var
     */
    private $apiUrl = self::API_URL;

    /**
     * Версия API
     *
     * @var int
     */
    private $timeout = self::TIMEOUT;

    /**
     * Конструктор
     *
     * @param array $config
     *
     * @throws APIClientException
     */
    public function __construct(array $config = [])
    {
        $this->setParameters($config);

        parent::__construct([
            'base_uri'        => $this->getApiUrl(),
            'timeout'         => $this->getTimeout(),
            'allow_redirects' => false,
            'headers'         => [
                'content-type'    => 'application/json',
                'user-agent'      => $this->getApplicationName().' / GuzzleHttp / '.$this->getApplicationId(),
            ]
        ]);
    }


    /**
     * Получить название приложения
     *
     * @return string
     */
    public function getApplicationName(): string
    {
        return $this->applicationName;
    }

    /**
     * Получить ID приложения
     *
     * @return string
     */
    public function getApplicationId(): string
    {
        return $this->applicationId;
    }

    /**
     * Получить секретный ключ приложения
     *
     * @return string
     */
    public function getApplicationSecret(): string
    {
        return $this->applicationSecret;
    }

    /**
     * Получить URL API сервера
     *
     * @return mixed
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * Получить максимальное время запроса
     *
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * Получить данные [GET]
     *
     * @param string $uri URI сегменты
     * @param array $filters
     */
    public function get(string $uri, array $filters = [])
    {
        $this->request('GET', $uri);
    }

    /**
     * Добавить запись на API сервер [POST]
     *
     * @param string $uri
     * @param array $data
     */
    public function add(string $uri, array $data = [])
    {
        $response = $this->post($uri, [
            'json' => $data
        ]);

        $response->getStatusCode();


//        $response->getBody()
        var_dump($response->getBody()->getContents());
    }

    /**
     * Редактирование записи [POST]
     *
     * @param string $uri
     * @param array $data
     */
    public function edit(string $uri, array $data = [])
    {

    }

    /**
     * Удаление записи [DELETE]
     *
     * @param string $uri
     * @param array $data
     */
    public function delete(string $uri, array $data = [])
    {

    }

    /**
     * Установка параметров API
     *
     * @param array $config
     *
     * @throws APIClientException
     */
    private function setParameters(array $config)
    {
        foreach ($config as $property => $value) {
            if ($keyName = $this->propertyNameFromMap($property)) {
                $this->{$keyName} = $value;
            }
        }

        if (!$this->applicationName || !$this->applicationId || !$this->applicationSecret) {
            throw new APIClientException('API parameters - application_name, application_id and application_secret required!');
        }
    }

    /**
     * Маппер для получения имен
     * @param string $property
     *
     * @return string|false
     */
    private function propertyNameFromMap(string $property)
    {
        return array_search($property, [
            'applicationName'   => 'application_name',
            'applicationId'     => 'application_id',
            'applicationSecret' => 'application_secret',
            'apiUrl'            => 'api_url',
            'apiVersion'        => 'api_version',
            'timeout'           => 'timeout',
        ], true);
    }
}